package com.tpo.util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class wedDriverUtil {


    public static WebDriver driver = null;
    //Declare Variables That To Use In Other Files Also
    public static String curr_dir = System.getProperty("user.dir");
    public static Properties props;

    public static String cust_dwn_pth = System.getProperty("user.home") + "\\Documents\\TPOExports";

    public static Map<String, Object> prefs;
    public static JavascriptExecutor js;

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(WebDriver driver) {
        wedDriverUtil.driver = driver;
    }

    public static void openDrowser()  {

        //Read Daya From .properties File
        try {
            FileReader reader=new FileReader(curr_dir + "\\Properties\\conf.properties");
             props=new Properties();
            props.load(reader);
        }catch (IOException e){

        }
        //--------------------------------

        //Create Chrome Driver With Options
       // ChromeOptions web_options = new ChromeOptions();
        //web_options.addArguments("--incognito");

        //driver = new FirefoxDriver();
        //driver = new ChromeDriver();



        //driver.get("http://passive.totalpatentone.com");


        js = (JavascriptExecutor) driver;

        EdgeOptions options = new EdgeOptions();

        prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory", cust_dwn_pth);
        prefs.put("download.prompt_for_download",false);
        prefs.put("download.directory_upgrade",true);
        prefs.put("safebrowsing.enabled",true);


        options.setExperimentalOption("prefs",prefs);
        options.addArguments("--disable-extensions");

        System.setProperty("webdriver.edge.driver","C:\\Projects\\edgedriver_win64\\111\\msedgedriver.exe");
        driver = new EdgeDriver(options);


            //Maximize The Window
        driver.manage().window().maximize();

            //Delete Cookies
        driver.manage().deleteAllCookies();

            //Set Timeouts
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        //-------------------------------
    }



}
