

package com.tpo.libraries.WORD;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.tpo.businessComponent.tpoLibCommon.*;
import static com.tpo.businessComponent.tpolibHome.search_query;
import static com.tpo.util.wedDriverUtil.driver;

public class WORD_6 {

    //Close Driver After Test Case Run
    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void documentViewValidation() throws InterruptedException, IOException, ParserConfigurationException, SAXException {

        //Get user Data From Excel
        String user_data = get_user_data();
        String[] user_data_array = user_data.split("@@@");
        String got_uname = user_data_array[0];
        String got_pwd = user_data_array[1];

        //Login To TPO
        login_to_tpo(got_uname, got_pwd);


        //Perform Search Query
        search_query("PN:(FR2260800A1)");


        Thread.sleep(4000);


        //Start testing Part
        //Click Different Search Results And Validate That Selected Search Result Details Displaying Or Not
        driver.findElement(By.cssSelector(".drop-down-button-chevron")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(3) .drop-down-list-item-link")).click();

        Thread.sleep(4000);

        driver.findElement(By.cssSelector(".grid-view-tile:nth-child(1) .grid-view-tile-heading")).click();

        Thread.sleep(3000);

        //WebElement myelement = driver.findElement(By.xpath("//div[@class='publication-section publication-section-biblio-images ng-star-inserted']/div/h3[@class='publication-section-title']"));
        WebElement myelement = driver.findElement(By.xpath("//h2[@class='publication-view-header-title-text']"));

        String title_in_web = myelement.getText();

        //Get Title From The Text
        String[] title_array = title_in_web.split("-");

        String splitted_name = String.join("-", Arrays.copyOfRange(title_array,1,title_array.length));

        /*
        for(int i = 1; i<title_array.length; i++){

            splitted_name += ' ' + title_array[i];

        }
        */
        title_in_web = splitted_name;
        //System.out.println(title_in_web);

        Thread.sleep(3000);


        ////div[@class='publication-section publication-section-biblio-images ng-star-inserted']/div/h3
        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@class='publication-view-header-action']/button[@class='icon icon-close']")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector(".grid-view-tile:nth-child(1) lnip-checkbox")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@class='publication-view-header-action']/button[@class='icon icon-close']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@class='documentsarea-toolbar-left-cropped']/ul[@class='icon-toolbar documentsarea-toolbar-actions']/li[@class='icon-toolbar__icon-wrapper']/lnip-export-button/button")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@class='mat-menu-content ng-tns-c234-1']/div/button[@title='Custom field selection']")).click();
        Thread.sleep(3000);

        //Clicking The WORD Radio Button
        driver.findElement(By.xpath("//label[@for='exportFileTypeword']")).click();
        driver.findElement(By.xpath("//label[@for='exportFileTypeword_formatted_zip']")).click();

        driver.findElement(By.xpath("//label[@for='exportSelection1']")).click();
        driver.findElement(By.xpath("//label[@for='fileFormattingSingle']")).click();



        Thread.sleep(1000);

        WebElement checkbox_input_eng = driver.findElement(By.id("customLanguageeng"));
        WebElement checkbox_label_eng = driver.findElement(By.id("labelcustomLanguageeng"));
        if(checkbox_input_eng.isSelected()) {
            checkbox_label_eng.click();
        }


        WebElement checkbox_input_lop = driver.findElement(By.id("customLanguagelop"));
        WebElement checkbox_label_lop = driver.findElement(By.id("labelcustomLanguagelop"));
        if(!checkbox_input_lop.isSelected()) {
            checkbox_label_lop.click();
        }

        WebElement checkbox_input_ddl = driver.findElement(By.id("customLanguageddl"));
        WebElement checkbox_label_ddl = driver.findElement(By.id("labelcustomLanguageddl"));
        if(checkbox_input_ddl.isSelected()) {
            checkbox_label_ddl.click();
        }

        Thread.sleep(5000);


        //Go To Field Selection Tab
        driver.findElement(By.xpath("//button[@type='button']//span[text()='Field selection']")).click();
        Thread.sleep(1000);

        //Select All Fields To Export
        WebElement checkbox_all_field = driver.findElement(By.id("selectAllFields"));
        WebElement checkbox_label_all_field = driver.findElement(By.id("labelselectAllFields"));
        if(!checkbox_all_field.isSelected()) {
            checkbox_label_all_field.click();
        }


        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[@data-testid='export-submit-btn']")).click();
        Thread.sleep(1000);

        driver.findElement(By.xpath("//button[@class='toast-close-button icon icon-close']")).click();
        Thread.sleep(4000);


        //Go to export section

        driver.findElement(By.xpath("//a[@data-testid='download-page-link']")).click();
        Thread.sleep(3000);

        driver.findElement(By.xpath("//tr[@data-gm-u-index='0']//td/div/a")).click();
        Thread.sleep(3000);


        //Downloading Data
        File downloadDir = new File(cust_dwn_pth);
        File[] files = downloadDir.listFiles();
        File mostRecentFile = null;

        for (File file : files) {

            if (mostRecentFile == null || file.lastModified() > mostRecentFile.lastModified()) {

                mostRecentFile = file;

            }

        }


        String oldFileExtension = null;
        if (mostRecentFile != null) {


            Path mypath = mostRecentFile.toPath();
            String oldFileName = mypath.getFileName().toString();
            oldFileExtension = oldFileName.substring(oldFileName.lastIndexOf(".") + 1);

            File newFile = new File(cust_dwn_pth + "\\MyExport." + oldFileExtension);

            if (newFile.exists()) {
                newFile.delete();
            }

            mostRecentFile.renameTo(newFile);

        }

        //Export XML From ZIP File
        File downloadZipFile = new File(cust_dwn_pth+"\\MyExport.zip");
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(downloadZipFile));
        ZipEntry zipEntry;

        while((zipEntry = zipInputStream.getNextEntry()) != null){

            String entryName = zipEntry.getName();
            if(entryName.endsWith(".docx")){

                File extractedXmlFile = new File(cust_dwn_pth+"\\Myexport.docx");
                OutputStream outputStream = new FileOutputStream(extractedXmlFile);
                byte[] buffer = new byte[1024];
                int length;

                while ((length = zipInputStream.read(buffer)) > 0){
                    outputStream.write(buffer,0,length);
                }
                outputStream.close();
                break;

            }

        }

        zipInputStream.close();


        //Read The Document Title From Word File
        String file_read_val = get_word_file_title("MyExport.docx");
        //----------------

        //Send Titledata To Validate
        titledata_validation(title_in_web,file_read_val);
        //driver.findElement(By.cssSelector(".publication-view-header-action > .icon-close")).click();


        //Logout From TPO
        logout_frm_tpo();

    }
}

