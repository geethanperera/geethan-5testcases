package com.tpo.businessComponent;

import com.tpo.pages.tpoHome;
import com.tpo.util.wedDriverUtil;
import org.openqa.selenium.Keys;

public class tpolibHome extends wedDriverUtil {

    //Function To Perform Search Query Based On Given Search Parameter
    public static void search_query(String get_search_para) throws InterruptedException {

        driver.findElement(tpoHome.search_txt_area).click();
        driver.findElement(tpoHome.search_txt_area).sendKeys(get_search_para);
        Thread.sleep(3000);
        //js.executeScript("if(arguments[0].contentEditable === 'true') {arguments[0].innerText = '"+get_search_para+"'}", tpoHome.srch_bx_element);
        driver.findElement(tpoHome.search_txt_area).sendKeys(Keys.ENTER);
        Thread.sleep(3000);
        //driver.findElement(tpoHome.search_btn_submit).click();
        Thread.sleep(8000);
    }


}
