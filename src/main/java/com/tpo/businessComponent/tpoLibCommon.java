package com.tpo.businessComponent;

import com.opencsv.exceptions.CsvValidationException;
import com.tpo.util.wedDriverUtil;
import com.tpo.pages.tpoLogin;
import com.tpo.pages.tpoHome;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import org.testng.annotations.Test;


import java.util.List;
import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.Properties;

import static com.tpo.pages.tpoHome.top_user_dropdown;
import static com.tpo.pages.tpoHome.user_dropdown_signout;

import com.opencsv.CSVReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class  tpoLibCommon extends wedDriverUtil {


    //Function To open Web Browser
    public static void login_to_tpo(String get_tpo_lgn_uname , String get_tpo_lgn_pwd){

        //Open Web Driver To Load The Website
        wedDriverUtil.openDrowser();
        driver.get(props.getProperty("web_url"));
        driver.findElement(tpoLogin.tpo_lgn_uname).sendKeys(get_tpo_lgn_uname);
        driver.findElement(tpoLogin.tpo_lgn_pwd).sendKeys(get_tpo_lgn_pwd);
        driver.findElement(tpoLogin.tpo_lgn_sbmt_btn).click();

    }

    //Logout Function
    public static void logout_frm_tpo(){

        driver.findElement(top_user_dropdown).click();
        driver.findElement(user_dropdown_signout).click();

    }

    //Function To Read User Data From Local xlsx File And Return Values
    public static String get_user_data(){

        //Read Data From Excel File
        String u_username = null;
        String p_password = null;
        try {

            FileInputStream the_file = new FileInputStream(new File(curr_dir+"\\Data\\unm_n_pw.xlsx"));
            XSSFWorkbook workbook = new XSSFWorkbook(the_file);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Row first_row = sheet.getRow(0);
            //String u_name = first_row.getCell(0);
            u_username = String.valueOf(sheet.getRow(0).getCell(0));
            p_password = String.valueOf(sheet.getRow(0).getCell(1));

        } catch (Exception e) {
            e.printStackTrace();
        }


        //Returning The Merged String
        String return_val =  u_username+"@@@"+p_password;
        return return_val;

    }

    public static String dwnld_excel_data_read(String File_name,Integer Sheet_num, Integer Row_num, Integer Col_num){

        //Read Data From Excel File
        String Return_Val = null;
        try {

            FileInputStream the_file = new FileInputStream(new File(cust_dwn_pth+"\\"+File_name));
            XSSFWorkbook workbook = new XSSFWorkbook(the_file);
            XSSFSheet sheet = workbook.getSheetAt(Sheet_num);
            //Row first_row = sheet.getRow(Row_num);
            //String u_name = first_row.getCell(0);
            Return_Val = String.valueOf(sheet.getRow(Row_num).getCell(Col_num));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Return_Val;

    }

    public static String dwnld_csv_data_read(String File_name, Integer Row_num, Integer Col_num) throws FileNotFoundException {

        //Read Data From CSV File
        String csv_path =cust_dwn_pth+"\\"+File_name;
        String Return_Val = null;
        int rowIndex = Row_num;
        int columnIndex = Col_num;


        try(CSVReader reader = new CSVReader(new FileReader(csv_path))){

            String[] headers = reader.readNext();
            String[] row = null;

            for(int i = 0; i < rowIndex + 1; i++){
                row = reader.readNext();
            }

             Return_Val = row[columnIndex];

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        }


        return Return_Val;

    }

    public static String get_xml_file_title(String File_name) throws FileNotFoundException {

        //Read Data From CSV File
        String xml_path =cust_dwn_pth+"\\"+File_name;
        String Return_Val = null;


        try {

            File inputFile = new File(xml_path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            System.out.println("Root element : " +doc.getDocumentElement().getNodeName());

            NodeList nlist = doc.getElementsByTagName("invention-title");
            for (int i = 0 ; i < nlist.getLength(); i++){

                Node node = nlist.item(i);
                if(node instanceof Element){

                    Element element = (Element) node;
                    String id = element.getAttribute("id");
                    if(id.equals("title_eng")){
                        String value = element.getTextContent();
                        Return_Val = value;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Return_Val;
    }

    public static String get_word_file_title(String File_name) throws IOException {

        String word_path =cust_dwn_pth+"\\"+File_name;
        File wordfile = new File(word_path);
        FileInputStream fis = new FileInputStream(wordfile);
        XWPFDocument document = new XWPFDocument(fis);

        List<XWPFParagraph> paragraphs = document.getParagraphs();
        String title = "";
        for (XWPFParagraph para : paragraphs){

            String paratext = para.getText();
            if(paratext.startsWith("Title:")){
                int index = paratext.indexOf(":");
                if(index != -1){
                    title = paratext.substring(index+1).trim();
                    break;
                }

            }

        }
        document.close();
        fis.close();

        return title;

    }

    public  static String get_pdf_file_title(String File_name) throws IOException {

        String pdf_path =cust_dwn_pth+"\\"+File_name;

        PDDocument document = PDDocument.load(new File(pdf_path));

        PDFTextStripper stripper = new PDFTextStripper();

        String text = stripper.getText(document);

        //Find The Line Tat Contains "Title:" Tag
        String[] lines = text.split("\r?\\n");

        String title = "Title:";
        String titleVal = "";


        for (int i = 0; i < lines.length; i++){

            if(lines[i].contains(title)){

                titleVal = lines[i+1];
                break;

            }

        }

        document.close();

        return titleVal;




    }


    public static void titledata_validation(String web_title,String in_file_title){

        //Remove Leading And Trailing Spaces From Two Strings
        web_title = web_title.trim();
        in_file_title = in_file_title.trim();

        //Validate That Both Titles Are Matched Or not
        if(web_title.equalsIgnoreCase(in_file_title)){

            System.out.println("===============================================");
            System.out.println("Success - Titles Matched");
            System.out.println("===============================================");

        }else{
            System.out.println("===============================================");
            System.out.println("Error - Titles Unmatched");
            System.out.println("Title In Web : "+web_title);
            System.out.println("Title In File : "+in_file_title);
            System.out.println("===============================================");

        }


    }



}
