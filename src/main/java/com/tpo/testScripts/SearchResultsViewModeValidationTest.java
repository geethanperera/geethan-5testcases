package com.tpo.testScripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.List;

import static com.tpo.businessComponent.tpoLibCommon.*;
import static com.tpo.businessComponent.tpolibHome.search_query;
import static com.tpo.util.wedDriverUtil.driver;

public class SearchResultsViewModeValidationTest {

    //Close Driver After Test Case Run
    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void searchResultsViewModeValidation() throws InterruptedException {

      //Get user Data From Excel
      String user_data = get_user_data();
      String[] user_data_array = user_data.split("@@@");
      String got_uname = user_data_array[0];
      String got_pwd = user_data_array[1];

      //Login To TPO
      login_to_tpo(got_uname,got_pwd);

      //Perform Search Query
      search_query("car");

      //Start testing Part
        // Check Different View Modes That User Can View Of Search Results
      driver.findElement(By.cssSelector(".drop-down-button-chevron")).click();
      driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(2) .drop-down-list-item-link")).click();
      try {
          Thread.sleep(5000);
      } catch (InterruptedException e) {
          e.printStackTrace();
      }
      {
          List<WebElement> elements = driver.findElements(By.cssSelector(".rows-list-item"));
          assert(elements.size() > 0);
      }
      driver.findElement(By.cssSelector(".drop-down-button-chevron")).click();
      driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(3) .drop-down-list-item-link")).click();
      try {
          Thread.sleep(5000);
      } catch (InterruptedException e) {
          e.printStackTrace();
      }
      {
          List<WebElement> elements = driver.findElements(By.cssSelector(".grid-view-tile:nth-child(1) > .ng-star-inserted"));
          assert(elements.size() > 0);
      }
      {
          List<WebElement> elements = driver.findElements(By.cssSelector(".grid-view-tile:nth-child(1) > .grid-view-publication-tile"));
          assert(elements.size() > 0);
      }

        //Logout From TPO
        logout_frm_tpo();

  }

}
