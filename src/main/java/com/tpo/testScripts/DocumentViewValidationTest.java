package com.tpo.testScripts;

import com.tpo.businessComponent.tpoLibCommon;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;

import static com.tpo.businessComponent.tpoLibCommon.*;
import static com.tpo.businessComponent.tpolibHome.search_query;
import static com.tpo.util.wedDriverUtil.driver;

public class DocumentViewValidationTest {

    //Close Driver After Test Case Run
    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void documentViewValidation() throws InterruptedException {

        //Get user Data From Excel
        String user_data = get_user_data();
        String[] user_data_array = user_data.split("@@@");
        String got_uname = user_data_array[0];
        String got_pwd = user_data_array[1];

        //Login To TPO
        login_to_tpo(got_uname,got_pwd);

        //Perform Search Query
        search_query("car");

        //Start testing Part
        //Click Different Search Results And Validate That Selected Search Result Details Displaying Or Not
        driver.findElement(By.cssSelector(".drop-down-button-chevron")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(3) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".grid-view-tile:nth-child(1) > .ng-star-inserted")).click();

            Thread.sleep(3000);

        driver.findElement(By.cssSelector(".publication-view-header-action > .icon-close")).click();
        driver.findElement(By.cssSelector(".grid-view-tile:nth-child(2) > .ng-star-inserted")).click();

            Thread.sleep(3000);

        driver.findElement(By.cssSelector(".publication-view-header-action > .icon-close")).click();


        //Logout From TPO
        logout_frm_tpo();

    }


}
