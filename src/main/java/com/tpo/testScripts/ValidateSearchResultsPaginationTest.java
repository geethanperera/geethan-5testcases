package com.tpo.testScripts;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.List;

import static com.tpo.businessComponent.tpoLibCommon.*;
import static com.tpo.businessComponent.tpolibHome.search_query;
import static com.tpo.util.wedDriverUtil.driver;

public class ValidateSearchResultsPaginationTest {

    //Close Driver After Test Case Run
    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void validateSearchResultsPaginationTest() throws InterruptedException {

        //Get user Data From Excel
        String user_data = get_user_data();
        String[] user_data_array = user_data.split("@@@");
        String got_uname = user_data_array[0];
        String got_pwd = user_data_array[1];

        //Login To TPO
        login_to_tpo(got_uname,got_pwd);

        //Perform Search Query
        search_query("car");

        //Start testing Part
        //Click On Different Pagination And Validate That Selected Page Number Will Display
        driver.findElement(By.cssSelector(".pagination-page:nth-child(5) > .ng-star-inserted")).click();

            Thread.sleep(4000);

        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".pagination-page-current > .ng-star-inserted"));
            assert(elements.size() > 0);
        }
        Assert.assertEquals(driver.findElement(By.cssSelector(".pagination-page-current > .ng-star-inserted")).getText(),"5");
        driver.findElement(By.cssSelector(".pagination-page:nth-child(2) > .ng-star-inserted")).click();

            Thread.sleep(4000);

        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".pagination-page-current > .ng-star-inserted"));
            assert(elements.size() > 0);
        }
        Assert.assertEquals(driver.findElement(By.cssSelector(".pagination-page-current > .ng-star-inserted")).getText(),"2");
        driver.findElement(By.cssSelector(".pagination-page:nth-child(6) > .ng-star-inserted")).click();

            Thread.sleep(4000);

        {
            List<WebElement> elements = driver.findElements(By.cssSelector(".pagination-page-current > .ng-star-inserted"));
            assert(elements.size() > 0);
        }
        Assert.assertEquals(driver.findElement(By.cssSelector(".pagination-page-current > .ng-star-inserted")).getText(), "6");
        driver.findElement(By.cssSelector(".pagination-pages-input")).click();
        driver.findElement(By.cssSelector(".pagination-pages-input")).sendKeys("6");
        driver.findElement(By.cssSelector(".pagination-pages-input")).sendKeys(Keys.ENTER);

        //Logout From TPO
        logout_frm_tpo();

    }
}
