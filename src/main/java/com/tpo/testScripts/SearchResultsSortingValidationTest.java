package com.tpo.testScripts;

import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static com.tpo.businessComponent.tpoLibCommon.*;
import static com.tpo.businessComponent.tpolibHome.search_query;
import static com.tpo.util.wedDriverUtil.driver;

public class SearchResultsSortingValidationTest {

    //Close Driver After Test Case Run
    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void searchResultsSortingValidationTest() throws InterruptedException {

        //Get user Data From Excel
        String user_data = get_user_data();
        String[] user_data_array = user_data.split("@@@");
        String got_uname = user_data_array[0];
        String got_pwd = user_data_array[1];

        //Login To TPO
        login_to_tpo(got_uname,got_pwd);

        //Perform Search Query
        search_query("car");

        //Start testing Part
        //Click On Different Sorting Options And Validate Those Will Display
        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(1) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(2) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(3) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(4) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(5) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(6) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(7) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(8) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(9) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(10) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(11) .drop-down-list-item-link")).click();

            Thread.sleep(4000);

        driver.findElement(By.cssSelector(".querysort-dropdown-content > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".drop-down-list-item:nth-child(12) .drop-down-list-item-link")).click();

            Thread.sleep(4000);


        //Logout From TPO
        logout_frm_tpo();


    }

}
